#define CATCH_CONFIG_MAIN

#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

string full_name(const string& fname, const string& lname)
{
    return fname + " " + lname;
}

TEST_CASE("reference binding")
{
    SECTION("Cpp98")
    {
        SECTION("l-value can be bound to l-value ref")
        {
            string first_name = "Jan";
            string& name_ref = first_name;
        }

        SECTION("r-value can be bound to const l-value ref")
        {
            const string& name_ref = full_name("Jan", "Kowalski");
        }
    }

    SECTION("Cpp11")
    {
        SECTION("r-value can be bound to r-value ref")
        {
            string&& name_rref = full_name("Jan", "Kowalski");

            cout << name_rref << endl;

            name_rref.clear();

            REQUIRE(name_rref.size() == 0);
        }

        SECTION("l-value can't be bound to r-value ref")
        {
            string name = "Jan";
            //string&& name_rref = name; //
        }
    }
}

template <typename T>
class MyContainer
{
    vector<T> vec_;

public:
    using value_type = typename vector<T>::value_type;
    using iterator   = typename vector<T>::iterator;
    typedef typename vector<T>::const_iterator const_iterator;

    MyContainer() = default;

    MyContainer(int size, T value = {})
        : vec_(size, value)
    {
    }

    MyContainer(std::initializer_list<T> il)
        : vec_{il}
    {
    }

    void add(const T& item)
    {
        cout << "add - copy " << item << endl;
        vec_.push_back(item);
    }

    void add(T&& item)
    {
        cout << "add - move " << item << endl;
        vec_.push_back(std::move(item));
    }

    iterator begin()
    {
        return vec_.begin();
    }

    iterator end()
    {
        return vec_.end();
    }

    const_iterator begin() const
    {
        return vec_.begin();
    }

    const_iterator end() const
    {
        return vec_.end();
    }
};

class Object
{
    char* buffer_ = nullptr;
    size_t size_{};
    std::string name_ = "object";
    friend ostream& operator<<(ostream& out, const Object& o);
public:
    Object() = default; // user declared

    Object(const string& name, char c = '#', size_t size = 10)
        : buffer_{new char[size]}, size_{size}, name_{name}
    {
        fill_n(buffer_, size_, c);
    }

    Object(Object&& source) noexcept
        : buffer_{std::move(source.buffer_)}, size_{std::move(source.size_)},
          name_{std::move(source.name_)}
    {
        cout << "Object(mvctor: " << name_ << ")\n";
        source.buffer_ = nullptr;
    }

    Object& operator=(Object&& source) noexcept
    {
        cout << "Object op=(mv: " << source.name_ << ")\n";

        if (this != &source)
        {
            delete [] buffer_;
            buffer_ = std::move(source.buffer_);
            size_ = std::move(source.size_);
            name_ = std::move(source.name_);
            source.buffer_ = nullptr;
        }

        return *this;
    }

    Object(const Object& source)
        : buffer_{new char[source.size_]}, size_{source.size_}, name_{source.name_}
    {
        cout << "Object(cctor: " << name_ << ")\n";
        copy(source.buffer_, source.buffer_ + size_, buffer_);
    }

    void swap(Object& other)
    {
        std::swap(buffer_, other.buffer_);
        std::swap(size_, other.size_);
        name_.swap(other.name_);
    }

    Object& operator=(const Object& source)
    {
        cout << "Object op=(" << name_ << ")\n";

        if (this != &source)
        {
            Object temp(source);
            swap(temp);
        }

        return *this;
    }

    ~Object()
    {
        delete [] buffer_;
    }

    void draw() const
    {
        cout << name_ << " : ";
        for(size_t i = 0; i < size_; ++i)
            cout << buffer_[i];
        cout << endl;
    }
};

ostream& operator<<(ostream& out, const Object& o)
{
    cout << "Object(" << o.name_ << ")";

    return out;
}

TEST_CASE("MyContainer - adding items")
{
    MyContainer<string> mc;

    string name = "Adam";
    mc.add(name);

    mc.add("cstring");
    mc.add(string("test"));
    mc.add(full_name("Jan", "Kowalski"));
}

TEST_CASE("Object tests")
{
    Object o1{"o1"};
    o1.draw();

    {
        Object o2 = o1;
        o2.draw();
    }

    o1.draw();

    Object o3 = std::move(o1);

    SECTION("can be stored in container")
    {
        MyContainer<Object> mc;

        mc.add(std::move(o3));
    }
}

class GameObject
{
    Object obj_;
    vector<Object> data_;
public:
    GameObject(size_t size) : obj_{"main"}
    {
        for(size_t i = 0; i < size; ++i)
            data_.push_back(Object{"obj"s + to_string(i + 1)});
    }
};

GameObject create_game()
{
    GameObject go(100);

    return go;
}

TEST_CASE("GameObject tests")
{
    cout << "\n\n";

    GameObject go = create_game();
}
