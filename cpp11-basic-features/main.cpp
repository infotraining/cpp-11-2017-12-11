#define CATCH_CONFIG_MAIN

#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("new large int type")
{
    long long x = 61235276345LL;

    auto l = 1738621783612LL; // long long int
    auto y = 273462783652837ULL; // unsigned long long int

    REQUIRE(sizeof(y) >= 8);
}

TEST_CASE("new aliases for integer types")
{
    char c = 22;

    int8_t sx  = 22; // signed char
    uint32_t x = 421; // undigned int (32bit)
}

void use(int* ptr)
{
    if (ptr)
        cout << "use(int*): " << (*ptr) << endl;
    else
        cout << "use(int* = 0)" << endl;
}

void use(long value)
{
    cout << "use(long): " << value << endl;
}

void use(nullptr_t) = delete;
void use(double)    = delete;

TEST_CASE("nullptr")
{
    SECTION("C++98")
    {
        int* ptr  = NULL;
        int* ptr2 = 0;

        int x = 10;

        use(&x);
        use(NULL);
    }

    SECTION("C++11")
    {
        int* ptr = nullptr;
        // use(nullptr); // error - use(nulptr_t) is deleted

        if (ptr == nullptr)
            cout << "value of ptr is 0" << endl;

        if (ptr)
            cout << "value of ptr is not 0" << endl;

        cout << "ptr: " << ptr << endl;
        cout << "ptr: " << reinterpret_cast<long>(nullptr) << endl;
    }

    SECTION("implicit conversions")
    {
        // use(8.11); // error use(double) is deleted
    }
}

TEST_CASE("Raw string literal")
{
    string path = R"(c:\nasz folder\text)";

    cout << path << endl;

    string lines = R"(line1
line2
line3)";

    REQUIRE(lines == "line1\nline2\nline3");

    SECTION("custom delimiters")
    {
        string note = R"raw(text"(text)"text)raw";

        cout << note << endl;
    }
}

template <typename T>
void deduce_type(T obj)
{
}

template <typename T>
void deduce_type(T* obj)
{
}

TEST_CASE("auto")
{
    auto x  = 42; // int
    auto c  = 'a'; // char
    auto dx = 3.14; // double

    SECTION("auto with iterators")
    {
        SECTION("C++98")
        {
            set<int, greater<int>> vec;

            vec.insert(1);
            vec.insert(2);
            vec.insert(3);

            for (set<int, greater<int>>::const_iterator it = vec.begin(); it != vec.end(); ++it)
            {
                cout << *it << " ";
            }
            cout << endl;
        }

        SECTION("C++11")
        {
            vector<int> vec(100);
            iota(vec.begin(), vec.end(), 1);

            for (auto it = vec.cbegin(); it != vec.cend(); ++it) // for(vector<int>::const_iterator it ...
            {
                cout << *it << " ";
            }
            cout << endl;
        }
    }

    SECTION("auto can be used with const, * and &")
    {
        auto g        = 9.81; // const double
        const auto id = "Square"; // const char*

        const auto* ptr1 = &g; // const double*
        static_assert(is_same<decltype(ptr1), const double*>::value, "error");

        auto ptr2 = &g;

        deduce_type(7.444); // deduce_type<double>(double)
        deduce_type(&g); // deduce_type<const double>(const double*)

        auto& ref_g = g;
    }
}

TEST_CASE("auto rules")
{
    int x        = 10;
    int* ptrx    = &x;
    int& refx    = x;
    const int cx = 20;
    int tab[]    = {1, 2, 3};

    SECTION("rule 1 - auto with ref")
    {
        auto& aref1  = refx; // int&
        auto& aref2  = cx; // const int&
        auto& reftab = tab; // int(&)[3]
    }

    SECTION("rule 3 - no ref and pointer in auto declaration")
    {
        auto ax1  = refx; // int - ref is stripped
        auto ax2  = cx; // int - const is stripped
        auto atab = tab; // int* - decay to pointer
    }

    SECTION("exception in the rule")
    {
        auto data = {1, 2, 3}; // std::initializer_list<int>

        vector<int> vec = {1, 2, 3, 4};

        vec.insert(vec.end(), data);
    }
}

TEST_CASE("auto issue")
{
    int a = 10;

    auto b1 = a;
    auto b2(a); // int
    auto b3 = {a}; // std::initializer_list<int>
    auto b4{a}; //C++11/14 - std::initializer_list<int> / C++17 - int
}

TEST_CASE("range-based for")
{
    SECTION("iterates over std containers")
    {
        vector<int> vec = {1, 2, 3};

        for (int item : vec)
        {
            cout << item << " ";
        }
        cout << endl;

        SECTION("is interpreted")
        {
            for (auto it = vec.begin(); it != vec.end(); ++it)
            {
                int item = *it;
                cout << item << " ";
            }
            cout << endl;
        }
    }

    SECTION("preffer references as variable type")
    {
        vector<string> words = {"one", "two", "three"};

        for (const string& word : words)
            cout << word << " ";
        cout << endl;

        SECTION("is interpreted")
        {
            for (auto it = words.begin(); it != words.end(); ++it)
            {
                const string& word = *it;
                cout << word << " ";
            }
            cout << endl;
        }
    }

    SECTION("works with auto")
    {
        vector<string> words = {"one", "two", "three"};

        for (const auto& word : words)
            cout << word << " ";
        cout << endl;

        SECTION("is interpreted")
        {
            for (auto it = words.begin(); it != words.end(); ++it)
            {
                const auto& word = *it;
                cout << word << " ";
            }
            cout << endl;
        }
    }

    SECTION("works with native arrays")
    {
        int tab[] = {1, 2, 3, 4, 5};

        for (const auto& item : tab)
            cout << item << " ";
        cout << endl;

        SECTION("is interpreted")
        {
            for (auto it = begin(tab); it != end(tab); ++it)
            {
                const auto& item = *it;
                cout << item << " ";
            }
            cout << endl;
        }
    }

    SECTION("works with initializer lists")
    {
        for (const auto& item : {1, 2, 3})
        {
            cout << item << " ";
        }
        cout << endl;
    }
}

namespace my_std
{
    template <typename T, size_t N>
    T* end(T (&array)[N])
    {
        return &array[N];
    }
}

TEST_CASE("begin() and end() functions")
{
    int tab[10] = {1, 2, 3, 4, 5};

    auto it1 = begin(tab); // int* it1 = &tab[0];
    auto it2 = end(tab); // int* it2 = &tab[10];
}

enum Coffee : uint8_t; // forward declaration

enum Coffee : uint8_t
{
    espresso = 65,
    cappucino,
    latte
};

void foo(uint8_t value)
{
    cout << "foo(value: " << value << ")" << endl;
}

TEST_CASE("enums in C++11")
{
    Coffee c;
    c = espresso;

    REQUIRE(sizeof(c) == 1);

    foo(c);
}

enum class DayOfWeek : uint64_t
{
    mon = 1,
    tue = 1,
    wed,
    thd,
    fri,
    sat,
    sun
};

TEST_CASE("scoped enums in C++11")
{
    DayOfWeek day = DayOfWeek::sun;

    foo(static_cast<uint8_t>(day));

    day = static_cast<DayOfWeek>(1);

    REQUIRE(day == DayOfWeek::tue);
    REQUIRE(day == DayOfWeek::mon);

    auto value = static_cast<underlying_type<DayOfWeek>::type>(day);
}

struct Aggregate
{
    int a, b;
    double c;
};

struct Point
{
    int x, y;

    Point(int x, int y)
        : x{x}
        , y{y}
    {
    }
};

class MyContainer
{
    vector<int> vec_;

public:
    using value_type = vector<int>::value_type;
    using iterator   = vector<int>::iterator;
    typedef vector<int>::const_iterator const_iterator;

    MyContainer(int size, int value = 0)
        : vec_(size, value)
    {
    }

    MyContainer(std::initializer_list<int> il)
        : vec_{il}
    {
    }

    iterator begin()
    {
        return vec_.begin();
    }

    iterator end()
    {
        return vec_.end();
    }

    const_iterator begin() const
    {
        return vec_.begin();
    }

    const_iterator end() const
    {
        return vec_.end();
    }
};

TEST_CASE("uniform initialzation")
{
    int zero{};
    int v1{5};
    const int cv1{13};
    int tab1[] = {1, 2, 3, 4};
    Aggregate a1{1, 2, 3.14};
    Point pt{1, 10};
    vector<int> vec       = {1, 2, 3, 4};
    vector<Point> points  = {{1, 2}, {3, 5}};
    map<int, string> dict = {{1, "one"}, {2, "two"}, {3, "three"}};

    int x{static_cast<int>(3343.14)};
    char c{x}; // only warning - clang reports an error
    double pi{3};

    SECTION("using initializer list")
    {
        MyContainer mc = {4, 2};

        for (auto& item : mc)
        {
            cout << item << " ";
        }
        cout << endl;
    }

    SECTION("constructor with initializer list is preffered")
    {
        vector<int> vec1{4, 2}; // [4, 2]
        vector<int> vec2(4, 2); // [2, 2, 2, 2]
    }

    int px{};
    double dx{};
    bool bx{};
    int* ptr{};
}

template <typename ValueT>
using Dictionary = std::map<int, ValueT>;

TEST_CASE("type aliases")
{
    Dictionary<string> dict = {{1, "one"}, {2, "two"}, {3, "three"}}; // map<int, string>
}

TEST_CASE("decltype")
{
    map<int, string> dict = {{1, "one"}, {2, "two"}};

    SECTION("can be used as variable type")
    {
        decltype(dict) dict_clone;

        REQUIRE(dict_clone.size() == 0);
    }

    SECTION("can be used in any place where type is required")
    {
        decltype(dict)::value_type my_pair{5, "five"};

        REQUIRE(my_pair.first == 5);
    }
}

namespace Cpp98
{
    template <typename Container>
    typename Container::value_type sum(Container& data)
    {
        typedef typename Container::value_type ResultType;

        ResultType result = ResultType();

        for (typename Container::const_iterator it = data.begin(); it != data.end(); ++it)
            result += *it;

        return result;
    }
}

namespace Cpp11
{
    template <typename Container>
    struct SumTraits
    {
        using ResultType = typename Container::value_type;
    };

    template <typename T, size_t N>
    struct SumTraits<T[N]>
    {
        using ResultType = T;
    };

    template <size_t N>
    struct SumTraits<uint8_t[N]>
    {
        using ResultType = uint64_t;
    };

    template <typename Container, typename ResultType = typename SumTraits<Container>::ResultType>
    ResultType sum(Container& data)
    {
        ResultType result{};

        for(const auto& item : data)
            result += item;

        return result;
    }
}

namespace Cpp14
{
    template <typename Container>
    auto sum(Container& data)
    {
        using ValueType = typename decay<decltype(data[0])>::type;
        using ResultType = decltype(declval<ValueType>() + declval<ValueType>());

        ResultType result{};

        for(const auto& item : data)
            result += item;

        return result;
    }

    template<typename Map>
    decltype(auto) find_value(Map& map, const typename Map::key_type& key)
    {
        return map.at(key);
    }
}

TEST_CASE("sum tests")
{
    using namespace Cpp11;

    const vector<int> vec = {1, 2, 3};
    REQUIRE(sum(vec) == 6);

    uint8_t tab[] = { 100, 200, sum(vec) };
    auto result = sum(tab);

    REQUIRE(result == 306);
    REQUIRE(sizeof(result) == 8);
}

TEST_CASE("find value tests")
{
    using namespace Cpp14;

    map<int, string> dict = { {1, "one"}, {2, "two"} };

    auto& value = find_value(dict, 2);

    REQUIRE(value == "two");

    value = "dwa";

    REQUIRE(dict[2] == "dwa");
}
