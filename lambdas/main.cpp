#define CATCH_CONFIG_MAIN

#include <iostream>
#include <string>
#include <vector>
#include <functional>
#include <memory>

#include "catch.hpp"

using namespace std;

class Lambda_72354276345
{
public:
    void operator()(int x) const { cout << "lambda x = " << x << endl; }
};

TEST_CASE("simplest lambda")
{
    [](){}();

    auto l0 = [] { cout << "simplest lambda"; };
    l0();

    using MyLambda = decltype(l0);
    // MyLambda ml; // lambda can't be created using default constructor

    //Lambda_72354276345 ll1 = Lambda_72354276345{};
    auto l1 = [](int x) { cout << "lambda x = " << x << endl; };

    l1(10); // wywolanie lambdy
}

template <typename Func>
void call(Func f)
{
    f(10);
}

TEST_CASE("using lambda as parameter")
{
    call([](int x) { cout << "my lambda: x = " << x << endl; });
}

void print(int value)
{
    cout << "print: " << value << endl;
}

class Printer
{
    int counter_{};
public:
    void operator()(int value)
    {
        counter_++;
        cout << "Printer: " << value << " - called: " << counter_  << endl;
    }
};

void call_function(std::function<void(int)> f)
{
    f(888);
}

TEST_CASE("callable can be stored in std::function")
{
    std::function<void(int)> f;

    SECTION("can store and call function pointer")
    {
        f = &print;
        f(42);

        call_function(&print);
    }

    SECTION("can store and call functors")
    {
        Printer prn;
        f = std::ref(prn); // std::ref - avoids making a copy of prn

        f(45);
        f(665);

        call_function(std::ref(prn));

        f(999);
    }

    SECTION("can store and call lambdas")
    {
        f = [](int value) { cout << "printing from lambda: " << value << endl; };

        f(112);
        f(113);

        call_function([](int value) { cout << "printing from lambda: " << value << endl; });
    }
}

struct Gadget
{
    int id;
    string name;
};


TEST_CASE("lambdas can be used with std::algorithms")
{
    SECTION("find_if")
    {
        vector<int> vec = { 6, 2345, 23, 646, 35 };

        auto where = find_if(vec.begin(), vec.end(), [](int x) { return  x > 1000; });

        REQUIRE(*where == 2345);
    }

    SECTION("sorting")
    {
        vector<Gadget> gadgets = { {1, "ipad"}, {5, "surface"}, {2, "ipod"} };

        sort(gadgets.begin(), gadgets.end(), [](const Gadget& g1, const Gadget& g2) { return g1.id < g2.id; });

        SECTION("since C++14")
        {
            sort(gadgets.begin(), gadgets.end(), [](const auto& g1, const auto& g2) { return g1.id < g2.id; });
        }
    }
}

std::function<int()> create_lambda()
{
    static int counter = 0;

    return [] { return ++counter; };
}

TEST_CASE("captures")
{
    SECTION("by val")
    {
        int threshold = 100;

        auto predicate1 = [threshold](int x) { return x > threshold; };

        REQUIRE(predicate1(200));

        threshold = 0;
        REQUIRE_FALSE(predicate1(50));
    }

    SECTION("by ref")
    {
        int counter = 0;

        auto l = [&counter] { ++counter; };

        l();
        l();
        l();

        REQUIRE(counter == 3);
    }

    SECTION("statics are not captured")
    {
        auto l1 = create_lambda();
        auto l2 = create_lambda();

        l1();
        l2();

        REQUIRE(l1() == 3);
        REQUIRE(l2() == 4);
    }

    SECTION("placeholders")
    {
        string str;
        vector<int> vec = {0};
        int x, y;

        auto l1 = [=, &vec] { /**/ };
        auto l2 = [&, str] { /**/ };

        const vector<int>& const_view_vec = vec;
        auto l3 = [=, &const_view_vec] { const_view_vec[0]; };
    }

    SECTION("since C++14 - move semantics is supported")
    {
        std::unique_ptr<Gadget> ptr = std::make_unique<Gadget>();

        auto lmv = [ptr = std::move(ptr)] { cout << "id: " << ptr->id << endl; };

        lmv();
    }
}

std::function<int()> create_generator(int seed)
{
    return [seed]() mutable { return ++seed; };
}

class Lambda_56293642983
{
   int seed_;
public:
   Lambda_56293642983(int seed) : seed_{seed}
   {}

   int operator()() { seed_ += 2; return seed_; }
};

TEST_CASE("generator tests")
{
    auto g1 = create_generator(665);

    REQUIRE(g1() == 666);
    REQUIRE(g1() == 667);

    vector<int> vec(10);

    int seed = 1;
    generate(vec.begin(), vec.end(), [&seed]() -> int { seed += 2; return seed; });

    for(const auto& item : vec)
    {
        cout << item << " ";
    }
    cout << endl;
}
