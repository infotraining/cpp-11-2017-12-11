#define CATCH_CONFIG_MAIN

#include <iostream>
#include <string>
#include <vector>
#include <numeric>

#include "catch.hpp"

using namespace std;

std::tuple<int, int, double> calculate_stats(const vector<int>& data)
{
    auto min = *min_element(data.begin(), data.end());
    auto max = *max_element(data.begin(), data.end());
    auto avg = accumulate(data.begin(), data.end(), 0.0) / data.size();

    return make_tuple(min, max, avg);
}

TEST_CASE("test")
{
    vector<int> vec = { 16, 235, 234, 1, 34, 453 };

    auto result = calculate_stats(vec);

    cout << "min: " << get<0>(result) << endl;
    cout << "max: " << get<1>(result) << endl;
    cout << "avg: " << get<2>(result) << endl;

    SECTION("unpacking tuple")
    {
        int min, max;

        tie(min, max, ignore) = calculate_stats(vec);

        cout << "min: " << min << endl;
        cout << "max: " << max << endl;
    }
}

class Gadget
{
    int id_ = -1;
    string name_ = "unknown";
    string nickname_ = "";

    auto tied() const
    {
        return tie(id_, name_, nickname_);
    }
public:
    Gadget(int id,const string& name) :id_{id}, name_{name}
    {
        cout << "Gadget(" << id << ", " << name_ << ")" << endl;
    }

    Gadget() = default;

    virtual ~Gadget()
    {
        cout << "~Gadget(" << id_ << ", " << name_ << ")" << endl;
    }

    virtual void info() const
    {
        cout << "Gadget(" << id_ << ", " << name_ << ")\n";
    }

    virtual void use() const
    {
        cout << "using Gadget(" << id_ << ", " << name_ << ")\n";
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    bool operator==(const Gadget& other) const
    {
        return tied() == other.tied();
    }

    bool operator<(const Gadget& other) const
    {
        return tied() < other.tied();
    }
};
