#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <mutex>
#include <memory>

using namespace std;
using namespace Catch::Matchers;

class Gadget
{
    int id_ = -1;
    string name_ = "unknown";
public:
    Gadget(int id,const string& name) :id_{id}, name_{name}
    {
        cout << "Gadget(" << id << ", " << name_ << ")" << endl;
    }

    Gadget() = default;

    virtual ~Gadget()
    {
        cout << "~Gadget(" << id_ << ", " << name_ << ")" << endl;
    }

    virtual void info() const
    {
        cout << "Gadget(" << id_ << ", " << name_ << ")\n";
    }

    virtual void use() const
    {
        cout << "using Gadget(" << id_ << ", " << name_ << ")\n";
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }
};

TEST_CASE("RAII")
{
    mutex mtx;
    vector<int> vec;

    SECTION("unsafe code")
    {
        mtx.lock();

        vec.push_back(1);

        mtx.unlock();
    }

    SECTION("safe code with RAII")
    {
        lock_guard<mutex> lk{mtx}; // mtx.lock()

        vec.push_back(1);
    } // mtx.unlock()
}

namespace Legacy
{
    Gadget* get_gadget(const string& name)
    {
        static int id;
        return new Gadget(++id, name);
    }

    void use(Gadget* g)
    {
        if (g)
            g->use();
    }

    void use_and_destroy(Gadget* g)
    {
        if (g)
            g->use();

        delete g;
    }

    Gadget* create_gadgets()
    {
        Gadget* tab = new Gadget[10];
        return tab;
    }
}

TEST_CASE("using legacy(leaky) code")
{
    using namespace  Legacy;

    // 1 - leak
    get_gadget("ipad");

    // 2 - leak
    use(get_gadget("surface"));

    use_and_destroy(get_gadget("ipod"));

    // 3 - invalid pointer
    //Gadget g{1, "test"};
    //use_and_destroy(&g);

    // 4 - ub
    //use_and_destroy(create_gadgets());
}

namespace Cpp11
{
    std::unique_ptr<Gadget> get_gadget(const string& name)
    {
        static int id;
        return std::unique_ptr<Gadget>(new Gadget(++id, name));
    }

    namespace Cpp14
    {
        std::unique_ptr<Gadget> get_gadget(const string& name)
        {
            static int id;
            return std::make_unique<Gadget>(++id, name);
        }
    }

    void use(Gadget* g)
    {
        if (g)
            g->use();
    }

    void use_and_destroy(std::unique_ptr<Gadget> g)
    {
        if (g)
            g->use();
    }

    vector<Gadget> create_gadgets()
    {
        return vector<Gadget>(10);
    }
}


TEST_CASE("using Cpp11 code")
{
    cout << "\n\n";

    using namespace Cpp11;

    // 1 - no leak
    get_gadget("ipad");

    auto g1 = get_gadget("ipod");
    g1->use();
    (*g1).use();

    SECTION("ownership can be transfered using move")
    {
        unique_ptr<Gadget> g2 = move(g1);

        REQUIRE(g1.get() == nullptr);

        g2.reset(new Gadget(3, "surface"));

        cout << "end of section" << endl;

        Legacy::use_and_destroy(g2.release());
    }

    SECTION("using raw-pointer")
    {
        auto g3 = get_gadget("surface");
        use(g3.get());

        use_and_destroy(move(g3));
    }

    SECTION("smart ptrs can be stored in std containers")
    {
        cout << "\n\n";

        vector<unique_ptr<Gadget>> gadgets;

        gadgets.push_back(get_gadget("g1"));
        gadgets.push_back(get_gadget("g2"));
        gadgets.push_back(get_gadget("g3"));
        gadgets.push_back(get_gadget("g4"));
        gadgets.push_back(move(g1));

        for(const auto& g : gadgets)
            g->use();

        vector<unique_ptr<Gadget>> evens;
        vector<unique_ptr<Gadget>> odds;

        partition_copy(make_move_iterator(gadgets.begin()), make_move_iterator(gadgets.end()),
                       back_inserter(evens), back_inserter(odds),
                       [](const auto& g) { return g->id() % 2 == 0;});

        cout << "\nevens:\n";
        for(const auto& g : evens)
            g->use();


        cout << "\nodds:\n";
        for(const auto& g : odds)
            g->use();

    }

//    use_and_destroy(get_gadget("ipod"));

//    // 3 - invalid pointer
//    //Gadget g{1, "test"};
//    //use_and_destroy(&g);
}


namespace LegacyCode
{
    template <typename T>
    unique_ptr<T> my_make_unique()
    {
        return unique_ptr<T>(new T());
    }

    template <typename T, typename Arg>
    unique_ptr<T> my_make_unique(Arg&& arg)
    {
        return unique_ptr<T>(new T(std::forward<Arg>(arg)));
    }

    template <typename T, typename Arg1, typename Arg2>
    unique_ptr<T> my_make_unique(Arg1&& arg1, Arg2&& arg2)
    {
        return unique_ptr<T>(new T(std::forward<Arg1>(arg1), std::forward<Arg2>(arg2)));
    }

    template <typename T, typename Arg1, typename Arg2, typename Arg3>
    unique_ptr<T> my_make_unique(Arg1&& arg1, Arg2&& arg2, Arg3&& arg3)
    {
        return unique_ptr<T>(new T(std::forward<Arg1>(arg1), std::forward<Arg2>(arg2), std::forward<Arg3>(arg3)));
    }
}

namespace  Cpp11
{
    template <typename T, typename... Args>
    std::unique_ptr<T> my_make_unique(Args&&... args)
    {
        return unique_ptr<T>(new T(std::forward<Args>(args)...));
    }

    void print()
    {
        cout << endl;
    }

    template <typename Head, typename... Tail>
    void print(const Head& head, const Tail&... tail)
    {
        cout << head << " ";
        print(tail...);
    }

}

TEST_CASE("variadic templates")
{
    Cpp11::print(1, 3.14, "Test", string("Str"));
}
