#define CATCH_CONFIG_MAIN

#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

struct X
{
    size_t id = get_id();
    int value{};
    string text = "text";
    size_t length = text.size();

    X(int v, string t) : value{v}, text{t}
    {}

    X(string t) : text{t}
    {}

    X() = default;

    int get_value() = delete;

    static size_t get_id()
    {
        static size_t counter{};

        return ++counter;
    }
};

struct NoCopyable
{
    NoCopyable() = default;
    NoCopyable(const NoCopyable&) = delete;
    NoCopyable& operator=(const NoCopyable&) = delete;
};


TEST_CASE("default sepcial functions")
{
    X x;

    REQUIRE(x.value == 0);
    REQUIRE(x.text == "text");
    REQUIRE(x.length == 4);

    x.text = "text2";
    X copy_x = x;

    REQUIRE(copy_x.text == "text2");
}

TEST_CASE("special funtions can be deleted")
{
    const NoCopyable nc;

    // NoCopyable nc2 = nc; // error
}

struct Aggregate
{
    int a = 0;
    int tab[10];
};

TEST_CASE("aggregates")
{
    // Aggregate agg1 = { 10, { 1, 2, 3, 4 } }; // C++11 - error | C++14 - ok
}


class Gadget
{
    int id_ = -1;
    string name_ = "unknown";
public:
    Gadget() = default;
    Gadget(int id) : id_{id}
    {}
    Gadget(int id, const string& name) : id_{id}, name_{name}
    {}
    virtual ~Gadget() = default;

    virtual void play() const
    {
        cout << "Gadget(" << id_ << " is playing..." << endl;
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }
};

class SuperGadget : public Gadget
{
    mutable size_t internal_counter_ = 3;
public:
    using Gadget::Gadget; // inheritance of constructors

    SuperGadget(int id) : Gadget{100*id}
    {}

    void play() const final override
    {
        if (internal_counter_ <= 0)
        {
            cout << "Game Over" << endl;
        }
        else
        {
            cout << "SuperGadget " << id() << " with name " <<  name() << " is playing..." << endl;
            --internal_counter_;
        }
    }
};

TEST_CASE("inheriting constructors")
{
    SuperGadget sg{1, "ipad"};
    sg.play();

    SuperGadget sg2{3};
    Gadget& g = sg2;
    g.play();
    g.play();
    g.play();
    g.play();
    g.play();
}





